import React, { ChangeEvent } from "react";
import { useStores } from "../stores";
import { observer } from "mobx-react";

const File: React.FC = observer(() => {
  const { csvStore } = useStores();

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) {
      return;
    }

    const fileReader = new FileReader();
    fileReader.onload = () => {
      let text = fileReader.result;

      csvStore.parseCsv(text as string);
    };

    const file = e.target.files[0];
    fileReader.readAsText(file);
  };

  return (
    <>
      <h3>File</h3>
      <input type="file" accept="text/csv" onChange={onChange} />
    </>
  );
});

export default File;
