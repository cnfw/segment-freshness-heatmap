import React from "react";
import { observer } from "mobx-react";
import { useStores } from "../../stores";
import { Slider, Rail, Handles, Tracks, Ticks } from "react-compound-slider";
import { yearsAgo } from "../../helpers/dates";
import { SliderRail, Handle, Track, Tick } from "../SliderComponents";
import { sliderStyle } from "../../helpers/sliderStyle";

const DateRange: React.FC = observer(() => {
  const { csvStore } = useStores();

  return (
    <>
      <h4>
        Date Range{" "}
        <small>
          {new Date(csvStore.lowerDate).toLocaleDateString()} -{" "}
          {new Date(csvStore.upperDate).toLocaleDateString()}
        </small>
      </h4>
      {csvStore.data.length > 0 && (
        <Slider
          mode={1}
          step={86400000}
          domain={[yearsAgo(3), csvStore.minMax.max]}
          rootStyle={sliderStyle}
          onUpdate={(e) => {
            csvStore.updateDates(e);
          }}
          values={[
            csvStore.lowerDate - (csvStore.lowerDate % 86400000),
            csvStore.upperDate,
          ]}
        >
          <Rail>
            {({ getRailProps }) => <SliderRail getRailProps={getRailProps} />}
          </Rail>
          <Handles>
            {({ handles, getHandleProps }) => (
              <div className="slider-handles">
                {handles.map((handle) => (
                  <Handle
                    key={handle.id}
                    handle={handle}
                    domain={[yearsAgo(3), csvStore.minMax.max]}
                    getHandleProps={getHandleProps}
                  />
                ))}
              </div>
            )}
          </Handles>
          <Tracks left={false} right={false}>
            {({ tracks, getTrackProps }) => (
              <div className="slider-tracks">
                {tracks.map(({ id, source, target }) => (
                  <Track
                    key={id}
                    source={source}
                    target={target}
                    getTrackProps={getTrackProps}
                  />
                ))}
              </div>
            )}
          </Tracks>
          <Ticks count={3}>
            {({ ticks }) => (
              <div className="slider-ticks">
                {ticks.map((tick) => (
                  <Tick
                    key={tick.id}
                    tick={tick}
                    count={ticks.length}
                    format={(d) => new Date(d).toLocaleDateString()}
                  />
                ))}
              </div>
            )}
          </Ticks>
        </Slider>
      )}
      {csvStore.data.length === 0 && (
        <p>
          <em>Load a file first</em>
        </p>
      )}
    </>
  );
});

export default DateRange;
