import React from "react";
import { observer } from "mobx-react";
import { Slider, Rail, Handles, Tracks } from "react-compound-slider";
import { sliderStyle } from "../../helpers/sliderStyle";
import { SliderRail, Handle, Track } from "../SliderComponents";
import { useStores } from "../../stores";

const Blurriness: React.FC = observer(() => {
  const { mapControlStore } = useStores();

  return (
    <>
      <h4>Blurriness</h4>
      <Slider
        mode={1}
        step={1}
        domain={mapControlStore.blurrinessDomain}
        rootStyle={sliderStyle}
        onUpdate={(newValues) => {
          mapControlStore.updateBlurriness(newValues);
        }}
        values={[mapControlStore.blurriness]}
      >
        <Rail>
          {({ getRailProps }) => <SliderRail getRailProps={getRailProps} />}
        </Rail>
        <Handles>
          {({ handles, getHandleProps }) => (
            <div className="slider-handles">
              {handles.map((handle) => (
                <Handle
                  key={handle.id}
                  handle={handle}
                  domain={mapControlStore.blurrinessDomain}
                  getHandleProps={getHandleProps}
                />
              ))}
            </div>
          )}
        </Handles>
        <Tracks right={false}>
          {({ tracks, getTrackProps }) => (
            <div className="slider-tracks">
              {tracks.map(({ id, source, target }) => (
                <Track
                  key={id}
                  source={source}
                  target={target}
                  getTrackProps={getTrackProps}
                />
              ))}
            </div>
          )}
        </Tracks>
      </Slider>
    </>
  );
});

export default Blurriness;
