import React from "react";

import { observer } from "mobx-react";
import DateRange from "./DateRange";
import Blurriness from "./Blurriness";
import IntensityRadius from "./IntensityRadius";

const Options: React.FC = observer(() => {
  return (
    <>
      <Blurriness />
      <br />

      <IntensityRadius />
      <br />

      <DateRange />
    </>
  );
});

export default Options;
