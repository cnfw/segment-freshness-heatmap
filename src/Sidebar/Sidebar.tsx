import React from "react";

import { observer } from "mobx-react";

import File from "./File";
import Info from "./Info";
import Options from "./Options/Options";

const Sidebar: React.FC = observer(() => {
  return (
    <div className="sidebar">
      <File />
      <Info />

      <h3>Options</h3>
      <Options />

      <div style={{ position: "absolute", bottom: "5px" }}>
        <small>v0.2.1 - Built for Waze Ireland</small>
      </div>
    </div>
  );
});

export default Sidebar;
