import { observable, action, computed } from "mobx";
import { yearsAgo } from "../helpers/dates";

export default class CsvStore {
  @observable data: any[] = [];

  @observable upperDate: number = 0;
  @observable lowerDate: number = 0;

  @computed get minMax() {
    let stamps = this.data.map((o) => o.LastEdited).sort();

    let min = stamps[0];
    let max = stamps[stamps.length - 1];

    return { min, max };
  }

  @computed get mapPoints() {
    return this.data
      .filter(
        (segment) =>
          segment.LastEdited >= this.lowerDate &&
          segment.LastEdited <= this.upperDate
      )
      .map((segment) => {
        return [
          segment.Latitude,
          segment.Longitude,
          parseInt(segment.LastEdited),
        ];
      });
  }

  @action updateDates(newDates: readonly number[]) {
    this.lowerDate = newDates[0] - (newDates[0] % 86400000);
    this.upperDate = newDates[1] - (newDates[1] % 86400000);
  }

  @action parseCsv(csvText: string) {
    let newData = [];

    let lines = csvText.split("\n");
    let headers = lines[0].split(",").map((header) => header.replace(" ", ""));

    for (let i = 1; i < lines.length; i++) {
      let thisLine = lines[i].split(",");
      let rowObj: any = {};

      for (let j = 0; j < headers.length; j++) {
        if (!["LastEdited", "Longitude", "Latitude"].includes(headers[j])) {
          continue;
        }

        rowObj[headers[j]] = +thisLine[j];
      }

      if (
        !(
          (!(rowObj.LastEdited <= 0) && !(rowObj.LastEdited > 0)) ||
          (!(rowObj.Longitude <= 0) && !(rowObj.Longitude > 0)) ||
          (!(rowObj.Latitude <= 0) && !(rowObj.Latitude > 0))
        )
      ) {
        newData.push(rowObj);
      }
    }

    this.data = newData;

    let { min, max } = this.minMax;

    let aYearAgoEpoch = yearsAgo();

    this.lowerDate = min < aYearAgoEpoch ? aYearAgoEpoch : min;
    this.upperDate = max;
  }
}
