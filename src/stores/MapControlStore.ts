import { observable, action } from "mobx";

export default class MapControlStore {
  blurrinessDomain = [0, 40];
  @observable blurriness = 6;

  radiusDomain = [0, 50];
  @observable radius = 8;

  @action updateBlurriness(newValues: readonly number[]) {
    this.blurriness = newValues[0];
  }

  @action updateRadius(newValues: readonly number[]) {
    this.radius = newValues[0];
  }
}
