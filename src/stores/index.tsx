import { createContext, useContext } from "react";

import CsvStore from "./CsvStore";
import MapControlStore from "./MapControlStore";

export const stores = {
  csvStore: new CsvStore(),
  mapControlStore: new MapControlStore(),
};

export const StoreContext = createContext(stores);

export const useStores = () => {
  return useContext(StoreContext);
};
