import React from "react";

import HeatmapLayer from "react-leaflet-heatmap-layer";
import { useStores } from "../stores";
import { observer } from "mobx-react";

const LastEditedHeatmapLayer: React.FC = observer(() => {
  const { csvStore, mapControlStore } = useStores();

  const { mapPoints, lowerDate, upperDate } = csvStore;
  const { blurriness, radius } = mapControlStore;

  return (
    <HeatmapLayer
      points={mapPoints}
      blur={blurriness}
      radius={radius}
      max={upperDate - lowerDate}
      latitudeExtractor={(m: any) => m[0]}
      longitudeExtractor={(m: any) => m[1]}
      intensityExtractor={(m: any) => m[2] - lowerDate}
    />
  );
});

export default LastEditedHeatmapLayer;
