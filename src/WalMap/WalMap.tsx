import React, { PropsWithChildren } from "react";

import { Map, TileLayer } from "react-leaflet";
import LastEditedHeatmapLayer from "./LastEditedHeatmapLayer";

type WalMapProps = {};

const WalMap: React.FC<PropsWithChildren<WalMapProps>> = (
  props: PropsWithChildren<WalMapProps>
) => {
  return (
    <div className="map">
      <Map center={{ lat: 53.6, lng: -7 }} zoom={7}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <LastEditedHeatmapLayer />
        {props.children}
      </Map>
    </div>
  );
};

export default WalMap;
